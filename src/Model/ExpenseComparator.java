package Model;

import java.util.Comparator;

public class ExpenseComparator implements Comparator{
	public int compare(Object o1, Object o2){		
		double com1 = ((Company)o1).getOutcome();		
		double com2 = ((Company)o2).getOutcome();
		
		if (com1 > com2) {return 1;}		
		if (com1 < com2) {return -1;}		
		return 0;		
	}
}
