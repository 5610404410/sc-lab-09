package Model;

import java.util.Comparator;

public class Product implements Taxable,Comparable<Product>,Comparator<Product>{
	private String productName;
	private double price;
	
	public Product(String proN, double p){
		productName = proN;
		price = p;
	}
	
	public String getProductName(){
		return productName;
	}
	
	public double getPrice(){
		return price;
	}
	
	public double getTax(){
		return price * (0.07);
	}
	
	public int compareTo(Product other){
		if (price < other.price){return -1;}
		if (price > other.price){return 1;}
		return 0;
	}
	
	public int compare(Product o1, Product o2){		
		double pro1 = o1.getTax();		
		double pro2 = o2.getTax();
		
		if (pro1 > pro2) {return 1;}		
		if (pro1 < pro2) {return -1;}		
		return 0;		
	}
	
}
