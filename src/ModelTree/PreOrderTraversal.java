package ModelTree;

import java.util.ArrayList;

public class PreOrderTraversal implements Traversal{
public ArrayList<Node> traverse(Node node) {
		
		ArrayList<Node> listNode = new ArrayList<Node>();
		
		listNode.add(node);
		
		if(node.getLeft() != null){
			ArrayList<Node> a = traverse(node.getLeft());
			
			for (Node x : a){
				listNode.add(x);
			}
		}

		if (node.getRight() != null){
			ArrayList<Node> b = traverse(node.getRight());
			
			for (Node x : b){
				listNode.add(x);
			}
		}	
		
		return listNode;		
	}
}
