package ModelTree;

import java.util.ArrayList;

public class ReportConsole {
	public void display(Node root, Traversal traversal){
		
		ArrayList<Node> showTree = traversal.traverse(root);
		for (Node x : showTree){
			System.out.print(x.getValue()+" ");
		}
		System.out.println();
	}
}
