package ModelTree;

import java.util.ArrayList;

public interface Traversal {
	ArrayList<Node> traverse(Node node);
}
